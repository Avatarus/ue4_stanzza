#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "HttpModule.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "GISS_HTTP.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRequestResult, FString, Response);

UENUM(BlueprintType)
enum class EVerb : uint8
{
	GET,
	POST,
	PUT,
	DEL
};

UENUM(BlueprintType)
enum class EContentType : uint8
{
	x_www_form_urlencoded,
	json
};

UCLASS()
class STANZZA_API UGISS_HTTP : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
	FString AuthToken;

	void SendRequest(const FString& URL, EVerb Verb, EContentType ContentType, const FString& Content, bool bAuthorize, const FHttpRequestCompleteDelegate& Callback);
	
	FString GetContentTypeString(EContentType ContentType);

	// Authorization
	UFUNCTION(BlueprintCallable)
	void Authorize(const FString& Login, const FString& Password);
	void OnAuthResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	UPROPERTY(BlueprintAssignable)
	FOnRequestResult OnAuthResult;

	// Test request
	UFUNCTION(BlueprintCallable)
	void TestRequest();
	void OnTestResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	UPROPERTY(BlueprintAssignable)
	FOnRequestResult OnTestResult;
};


