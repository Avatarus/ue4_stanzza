#include "GISS_HTTP.h"
#include "Json.h"

void UGISS_HTTP::SendRequest(const FString& URL, EVerb Verb, EContentType ContentType, const FString& Content, bool bAuthorize, const FHttpRequestCompleteDelegate& Callback)
{
    FHttpModule& httpModule = FHttpModule::Get();
    TSharedRef<IHttpRequest, ESPMode::ThreadSafe> Request = httpModule.CreateRequest();
    Request->SetURL(URL);
    Request->SetVerb(UEnum::GetDisplayValueAsText(Verb).ToString());
    Request->SetHeader(TEXT("Content-Type"), GetContentTypeString(ContentType));
    if (!Content.IsEmpty())
        Request->SetContentAsString(Content);
    if (bAuthorize)
        Request->SetHeader(TEXT("Authorization"), "Bearer " + AuthToken);
    Request->OnProcessRequestComplete() = Callback;
    Request->ProcessRequest();
}

FString UGISS_HTTP::GetContentTypeString(EContentType ContentType)
{
    switch (ContentType)
    {
        case EContentType::x_www_form_urlencoded: return TEXT("application/x-www-form-urlencoded");
        case EContentType::json: return TEXT("application/json");
    }
    return FString();
}

void UGISS_HTTP::Authorize(const FString& Login, const FString& Password)
{
    FString URL = TEXT("https://stanzza-api.aicrobotics.ru/api/auth/login");
    FString Fingerprint = FPlatformMisc::GetHashedMacAddressString();

    TSharedRef<FJsonObject> JsonObject = MakeShareable(new FJsonObject);
    JsonObject->SetStringField("login", Login);
    JsonObject->SetStringField("password", Password);
    JsonObject->SetStringField("fingerprint", Fingerprint);

    FString JsonString;
    TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&JsonString);
    FJsonSerializer::Serialize(JsonObject, Writer);

    SendRequest(URL, EVerb::POST, EContentType::json, JsonString, false, FHttpRequestCompleteDelegate::CreateUObject(this, &UGISS_HTTP::OnAuthResponse));
}

void UGISS_HTTP::OnAuthResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
    if (bWasSuccessful && Response.Get())
    {
        FString ResponseStr = Response->GetContentAsString();
        TSharedRef<TJsonReader<TCHAR>> JsonReader = FJsonStringReader::Create(ResponseStr);
        TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
        FJsonSerializer::Deserialize(JsonReader, JsonObject);
        AuthToken = JsonObject->GetStringField(TEXT("access_token"));
       
        OnAuthResult.Broadcast(ResponseStr);
    }
}

void UGISS_HTTP::TestRequest()
{
    FString URL = TEXT("https://stanzza-api.aicrobotics.ru/api/v1/catalog/tree");
    SendRequest(URL, EVerb::GET, EContentType::x_www_form_urlencoded, FString(), true, FHttpRequestCompleteDelegate::CreateUObject(this, &UGISS_HTTP::OnTestResponse));
}

void UGISS_HTTP::OnTestResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
    if (bWasSuccessful && Response.Get())
    {
        FString ResponseStr = Response->GetContentAsString();
        OnTestResult.Broadcast(ResponseStr);
    }
}